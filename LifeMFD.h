// ==============================================================
// LifeMFD for DeltaGlider, part of Orbiter RTMP
//   (c) 2021-... Boris Segret, PhD.
// v1.1 (alpha), open source under MIT License
// based on ORBITER and its SDK, by Dr. Martin Schweiger, open source since 2021
// based on CustomMFD, Part of the ORBITER SDK, by Dr. Martin Schweiger
// ==============================================================
/*
* NOTE: The "master" branch is WITHOUT GraphMFD class ("with-graphs" branch
*       keeps the use of the GraphMFD class)
* 
* F/ LIFE Support for one SAME vessel, until the Launchpad is exit or the module unselected.
*    The monitored vessel is the first focussed vessel while launching orbiter.
* 
*    At each step
*    - display felt acceleration in G (swgt), maxand averages over 5s., 1m, 7d
*    - feature "seat-belt fasten" (yet, always true), limits camera motion (not felt Gs)
*    Needed improvements:
*    - make it compatible with time-acceleration (although targeting real-time sim)
*    - generalize the landed status (yet it assumes weight = 1G i.e. Earth surface)
*    - LifeMFD::clbkPostStep not recognized, opcPostStep is used instead
*    - read the Resources properties from a scenario or a vessel, store and restore when exiting
*    Next desirable features:
*      > cam shaking should be added in 2D-panels as well
*      > black/red veil
*      > "LIFE" flag in HUD and 2D-views if limits are reached
*      > allow felt acc. & monitoring also in replay
*      > generalize camera movements for any location in plane (passengers, co-pil, seat-belt)
*/

#ifndef __LIFEMFD_H
#define __LIFEMFD_H

class LifeMFD: public MFD2 {
public:
	LifeMFD (DWORD w, DWORD h, VESSEL *vessel);
	~LifeMFD ();
	bool ConsumeKeyBuffered (DWORD key);
	bool ConsumeButton (int bt, int event);
	char *ButtonLabel (int bt);
	int  ButtonMenu (const MFDBUTTONMENU **menu) const;
	//void Update (HDC hDC);
	bool Update (oapi::Sketchpad *skp);
	//bool SetLogRange (char *rstr);
	//bool SetLinRange (char *rstr);
	void WriteStatus (FILEHANDLE scn) const;
	void ReadStatus (FILEHANDLE scn);
	void StoreStatus (void) const;
	void RecallStatus (void);
	static int MsgProc (UINT msg, UINT mfd, WPARAM wparam, LPARAM lparam);

private:
	oapi::Font* fontN;
	oapi::Font* fontB;
	oapi::Font* fontV;
	oapi::Brush* dfltBrsh;
	oapi::Brush* ylwBrush = 0;
	oapi::Brush* grnBrush = 0;
	oapi::Brush* redBrush = 0;
	oapi::Brush* bluBrush = 0;  // brushes (if graphic client)
	//long hh, ww; // height and average width of a character in the font
	oapi::Pen* redPen;
	oapi::Pen* warningPen;
	oapi::Pen* neutralPen;
	oapi::Pen* defaultPen;
	void paintDampingFlag(oapi::Sketchpad* skp);
	void paintGGauge(oapi::Sketchpad* skp);
	long remaingAir, remaingWater, remaingFood; // remaining [unit] resources
	long maxwoAir, maxwoWater, maxwoFood; // max [day] without resources
	long rateAir, rateWater, rateFood; // resources consomption [u/day]
	void paintAutonomyLevels(oapi::Sketchpad* skp);
	//bool  log_auto, lin_auto;
	int page = 0;
	//int mfd;

	// transient parameter storage
	static struct SavePrm {
		int page;
		float logmin, logmax;
		float linmin, linmax;
	} saveprm;
};

#endif //!__LIFEMFD_H