// ==============================================================
// LifeMFD for DeltaGlider, part of Orbiter RTMP
//   (c) 2021-... Boris Segret, PhD.
// v1.1 (alpha), open source under MIT License
// based on ORBITER and its SDK, by Dr. Martin Schweiger, open source since 2021
// based on CustomMFD, Part of the ORBITER SDK, by Dr. Martin Schweiger
// v. 1.1, 2022-12-24
// v. 1.0, 2022-10-27
// ==============================================================
/*

/* At the moment, the pilot location is as defined in DeltaGlider.cpp
SetCameraOffset(_V(0, 1.467, 6.782));
SetCameraShiftRange(_V(0, 0, 0.1), _V(-0.2, 0, 0), _V(0.2, 0, 0));
oapiVCSetNeighbours(1, 2, -1, -1);
*/

//#define STRICT
#define ORBITER_MODULE
#include "windows.h"
#include <stdio.h>
#include <math.h>
#include "orbitersdk.h"
#include "LifeMFD.h"

// ==============================================================
// Global variables

#define GWGREY 5 // G-Warnings (provision for next features)
#define DKYELLOW 0x00BBBB // Dark yellow
#define GWARNING 0x0000FF // Red
#define GWGREEN 1
#define SHOCKDT  0.1 // [s] below this duration, any non-constant acceleration is considered as a shock
#define GLIMIT00   8 // [G] Absolute affordable limit for acceleration
#define GLIMIT5S   2 // [G] Limit to prevent black/red veil without any G-suit or training
#define GLIMIT1H 1.3 // [G] Limit to prevent cardio accident without any G-suit or training
#define GLIMIT7D 0.8 // [G] Low limit to ensure good squeleton resilience without training
#define LENGTH1H 720 // size of the g1h array (720 = 1H/5s)
#define LENGTH7D 168 // size of the g7d array (168 = 7d/1H)

int module;  // module identifier of LifeMFD.dll (in LaunchPad, once for all)
const int ndata = 200;  // data points
const double sample_dt = 2.0;
int g_weight; // id of the graph for Weight plot
double lastGAssess = 0.;
VECTOR3 shakeCamOffset = { 0.,0.,0. };
bool prevShake = true;
bool seatBeltFasten = false;
bool preTrauma, traumaRisk, trauma, veilRisk, cardioRisk, skeletRisk; // veil = fainting = faintg
double dtVeil, dtCardio, ddSkelet;
long i1h = 0, i7d = 0;
double cumulG1h = 0, cumulG7d = 0;
double dwoR[3] = { -1,-1,-1 }; // [d], duration without resource [Air|Water|Food], evaluated in opcPostStep

static struct {  // Life MFD parameters, associated with 1 vessel
	VESSEL* vsl = NULL;
	double  refMass = 0; // [kg], will be intialized at first use
	double  gc = 0, g5s = 1; // [G], current (gc) and 5s-average accelerations
	double  g1h[LENGTH1H]; // [G], 5s-average accelerations
	double  g7d[LENGTH7D]; // [G], 1H-average accelerations
	double  Tc = 0, T5s = 0; // [s][s], current (Tc) and cumulated integration in gc, g5s
	double  nc,     n5s = 0, n1h = 0, n7d = 0; // [s][s][h][day], next epochs to update the received accelerations
	double  rgAir =  12, rgWater = 24, rgFood = 1; // [u], remaining Air, Water, Food
	double  cdAir =  24, cdWater = 24, cdFood = 1; // [u/day/pax], consumed Air|Water|Food per day and person
	double  woAir = .05, woWater =  1, woFood = 4; // [day*pax][day][day], without Air(shared by all)|Water|Food, duration to linear increase of risk from 0% to 100%
	long    nPax = 1; // Nb.of crew+passengers
	double  ftwoA = -1, ftwoW = -1, ftwoF = -1; // [mjd], first time without Air|Water|Food (<0 if no lack)
} life;


static struct {  // global data storage
	double tnext;   // time of next sample
	float* time;    // sample time
	float* weight;  // weight norm data in vessel axes
	int    sample;   // current sample index
	float* stime;   // sub-sample time
	int    ssample;  // current sub-sample index
	bool   oddSmpl;  // alternating sampling
	int color; // warning color in the graph (threashold, not used yet)
} g_Data;
float naV;

// ==============================================================
//                      Module's API interface
// ==============================================================


DLLCLBK void InitModule(HINSTANCE hDLL)
{
	// this is run only once at LifeMFD selection in LaunchPad (before scenario launch)

	static char *name = "LIFE Support";
	MFDMODESPECEX spec;
	spec.name    = name;
	spec.key     = OAPI_KEY_G;
	spec.context = NULL;
	spec.msgproc = LifeMFD::MsgProc;
	module = oapiRegisterMFDMode(spec); // Register the new MFD mode with Orbiter
												  // to be changed into VESSEL4::RegisterMFDMode

	life.vsl = NULL;
	g_Data.tnext = 0.0;
	g_Data.sample = 0;
	g_Data.time   = new float[ndata]; memset(g_Data.time,   0, ndata * sizeof(float));
	g_Data.weight = new float[ndata]; memset(g_Data.weight, 0, ndata * sizeof(float));

	g_Data.ssample = 0;
	g_Data.oddSmpl = false;

	g_Data.color = 3;
	shakeCamOffset = { 0.,0.,0. };
	prevShake = true;
}

DLLCLBK void ExitModule (HINSTANCE hDLL)
{
	// this is run only once at LifeMFD deselection or Exit (not even at scenario closing)
	oapiUnregisterMFDMode (module);
	delete[]g_Data.time;
	delete[]g_Data.stime;
	delete[]g_Data.weight;
}

// ==============================================================
//           update at each simulation timestep 
//                (whatever the MFDs)
// ==============================================================
DLLCLBK void opcPostStep(double simt, double simdt, double mjd) {
// simt [s], simdt[s], mjd [day]
// this member acts on the VC camera, not on the MFD mode
// a better implementation should be with clbkPostStep, but NOT in the LifeMFD class!!

// local declarations
	VESSEL* cV;
	VECTOR3 ww = { 0,-1,0}, ff, thV, acc, aMax;
	VECTOR3 currentCamOffset, newCamOffset, shakeLvl = { 0.,0.,0. }, Vpitch = { 0,0,1 };
	double m, wgt, tilt, th_main_level, naVmax;// , pitch;
	double rnd;
	double damping = 1.;
	double thRate;

// one single vessel is monitored (i.e. until module is unselected or Orbiter exited)
// vessel selected = first ever focusVessel

	if (life.vsl == NULL) {
		// actual initializarion occurs here for a given vessel
		life.vsl = oapiGetFocusInterface(); // first time a vessel gets a focus
		life.refMass = life.vsl->GetEmptyMass() + life.vsl->GetMaxFuelMass() / 2;
		life.nPax = 1;
		life.gc = 0;
		life.nc = simt;
		life.Tc = 0;
		preTrauma = false;
		traumaRisk = false;
		trauma = false;
		life.T5s = 0;
		life.n5s = simt;
		veilRisk = false;
		dtVeil = 0;
		life.n1h = mjd*24;
		cumulG1h = (double)LENGTH1H;  i1h = 0;
		// should be replaced by the local/stored load
		for (int i = 0; i < LENGTH1H; i++) { life.g1h[i] = 1; }
		cardioRisk = false;
		dtCardio = 0;
		life.n7d = mjd;
		cumulG7d = (double)LENGTH7D;  i7d = 0;
		// should be replaced by the local/stored load
		for (int i = 0; i < LENGTH7D; i++) { life.g7d[i] = 1; }
		skeletRisk = false;
		ddSkelet = 0;
		// duration without resources
		// dwoR[:] = { -1, -1, -1 };
    }
	cV = oapiGetFocusInterface(); // current vessel

	// computing the non-gravitational acceleration

	m = life.vsl->GetMass(); // [kg]

	// get the landed / not landed status:
	if (life.vsl->GetFlightStatus() && 0x1) {
			 ww = operator*= (ww, m * 9.81); // [N], yet assumed to be gravity at  Earth surface
	} else { life.vsl->GetWeightVector(ww); } // [N]}
	
	life.vsl->GetForceVector(ff); // [N]
	acc = operator/= (-(ff - ww), m * 9.81); // felt acceleration on board [G]
	naV = length(acc); // nb of Gs

	// UPDATES of the risks

	life.gc  = (life.gc * life.Tc + naV * simdt) / (life.Tc + simdt);
	life.Tc += simdt;
	life.g5s = (life.g5s * life.T5s + naV * simdt) / (life.T5s + simdt);
	life.T5s = min(5, life.T5s + simdt); // [s]
	if (simt >= life.nc) {
		// updating *** TRAUMA *** risk at every step (risk of short/long shocks)
		traumaRisk = (life.gc > GLIMIT00);
		trauma = (traumaRisk && preTrauma); // shorter than SHOCKDT is OK, longer is KO
		preTrauma = traumaRisk;
		life.Tc = 0; // reset short-term cumulative acc.
		// updating *** VEIL *** risk every SHOCKDT (black/red veil, blud pressure)
		life.nc  = simt + SHOCKDT;
		veilRisk = (life.g5s > GLIMIT5S);
		if (veilRisk) { dtVeil += SHOCKDT; } // [s], duration of the veil risk
		else { dtVeil = 0; } // reset
	}
	if (simt >= life.n5s) {
		life.n5s = simt + 5; // [s]
	// UPDATES of resources-related risks
		life.rgAir -= life.cdAir * life.nPax * 5 / 86400;
		if (life.rgAir <= 0) {
			life.rgAir = 0;
			if (life.ftwoA <0) life.ftwoA = mjd - 5 / 86400; // then stores the last mjd
			dwoR[0] = mjd - life.ftwoA; // update of the duration without the resource
		} // reset of life.FtwoA to -1 is a specific process (docking... resuply...)
		life.rgWater -= life.cdWater * life.nPax * 5 / 86400;
		if (life.rgWater <= 0) {
			life.rgWater = 0;
			if (life.ftwoW < 0) life.ftwoW = mjd - 5 / 86400; // then stores the last mjd
			dwoR[1] = mjd - life.ftwoW; // update of the duration without the resource
		}
		life.rgFood -= life.cdFood * life.nPax * 5 / 86400;
		if (life.rgFood <= 0) {
			life.rgFood = 0;
			if (life.ftwoF < 0) life.ftwoF = mjd - 5 / 86400; // then stores the current mjd
			dwoR[2] = mjd - life.ftwoF; // update of the duration without the resource
		}

	// updating *** CARDIO *** risk (1h) every 5s
		cumulG1h -= life.g1h[i1h];
		life.g1h[i1h] = life.g5s;
		cumulG1h += life.g1h[i1h];
		i1h += 1; if (i1h == LENGTH1H) i1h = 0;
		cardioRisk = (cumulG1h / LENGTH1H > GLIMIT1H);
		if (cardioRisk) { dtCardio += 5; } // duration of the cardio risk
		else { dtCardio = 0; } // reset
	}
	// updating *** SKELETON *** risk (7d) every 1h
	if (mjd*24 >= life.n1h) {
		life.n1h  = mjd*24 + 1;
		cumulG7d -= life.g7d[i7d];
		life.g7d[i7d] = cumulG1h / LENGTH1H;
		cumulG7d += life.g7d[i7d];
		i7d += 1;  if (i7d == LENGTH7D) i7d = 0;
		skeletRisk = (cumulG7d / LENGTH7D < GLIMIT7D);
		if (skeletRisk) { ddSkelet += 1; } // duration of the remaining Skeleton risk
		else { ddSkelet = max(0, ddSkelet-0.5); } // progressive reset (twice slower)
	}


	// changing the current camera if VC is running in the monitored vessel:
	if (cV == life.vsl && oapiCameraInternal()) {
		// useless otherwise
		life.vsl->GetCameraOffset(currentCamOffset); // current location of the camera
		tilt = max(min((acc.x) * 10, 30), -30) * PI / 180;
		//Vpitch = { 0, sin(pitch), cos(pitch) };
		life.vsl->SetCameraDefaultDirection(Vpitch, tilt);
		th_main_level = life.vsl->GetThrusterGroupLevel(THGROUP_MAIN);
		if (abs(th_main_level) > 0.1) {
			life.vsl->GetThrustVector(thV);
			thRate = length(thV) / (life.refMass * 9.81); // Thrust in Gs
			// damping is expected
			damping = 1 * pow(thRate,0.3) * (pow(thRate,0.8) - 1); // 0 at NO thrust and 1G thrust, max (~2) at full thrust
			rnd = (double)((int)(simt * 1000) % 10); // pseudo-random generation, 0..9
			shakeLvl = { 0.5, (rnd - 5.) / 5., 0. }; // shakeLvl.y in -0.5..0.5
			shakeLvl *= (damping * 0.001); // damping=1 converted in 1 millimeter
			if (prevShake) { shakeLvl *= -1; } // this alternates left/right
			prevShake = !prevShake;
		}
		else { shakeLvl = { 0., 0., 0. }; }

		// in addition of shakeLvl (0.001[m]) acc=1G is converted into 0.05[m]
		aMax = acc;
		if (seatBeltFasten) {
			naVmax = max(1, naV); // naV becomes 1 if acc.<1G
			aMax = operator/=(acc, naVmax); /* if acc.<1G, does nothing
											 if acc.>1G, seatBelt limits norm(acc) to 1, and keeps acc direction */
		}
		newCamOffset = aMax * 0.05 + shakeLvl;
		life.vsl->SetCameraOffset(currentCamOffset - shakeCamOffset + newCamOffset);
		shakeCamOffset = newCamOffset;
	}
}

// ==============================================================
//                LifeMFD mode implementation
// ==============================================================

LifeMFD::LifeMFD(DWORD w, DWORD h, VESSEL *vessel)
: MFD2 (w, h, vessel)
{
		//g_weight = AddGraph ();
	//SetAxisTitle (g_weight, 0, "Time: s");
	//SetAxisTitle (g_weight, 1, "Weight: G");
	//AddPlot (g_weight, g_Data.time, g_Data.weight, ndata, 3, &g_Data.sample); // see SelectDefaultPen (MFD2::GetDefaultPen )

	/*g = AddGraph();
	SetAxisTitle (g, 0, "Alt: km");
	SetAxisTitle (g, 1, "Pitch: deg");
	AddPlot (g, g_Data.alt, g_Data.pitch, ndata, 1, &g_Data.sample);*/

	//log_auto = true;
	//lin_auto = true;
	page = 0;

	// MFD initialisation here
	fontN = oapiCreateFont(w / 25, true, "Arial", FONT_NORMAL, 0); // WARNING, last param = orientation [1/10 deg]
	fontB = oapiCreateFont(w / 25, true, "Arial", FONT_BOLD  , 0); // WARNING, last param = orientation [1/10 deg]
	fontV = oapiCreateFont(w / 25, true, "Arial", FONT_NORMAL, 900); // WARNING, last param = orientation [1/10 deg]
	grnBrush = oapiCreateBrush(0x00FF00); // green (BBGGRR)
	ylwBrush = oapiCreateBrush(DKYELLOW); // dark yellow (BBGGRR)
	redBrush = oapiCreateBrush(GWARNING); // red (BBGGRR)
	bluBrush = oapiCreateBrush(0xFF0000); // blue (BBGGRR)
	//}
}

LifeMFD::~LifeMFD() {
	// MFD cleanup code here
	oapiReleaseFont(fontN);
	oapiReleaseFont(fontB);
	oapiReleaseFont(fontV);
	oapiReleaseBrush(grnBrush);
	oapiReleaseBrush(ylwBrush);
	oapiReleaseBrush(redBrush);
	oapiReleaseBrush(bluBrush);
}

// Return button labels
char* LifeMFD::ButtonLabel(int bt)
{
	// The labels for the two buttons used by our MFD mode
	char* label[12] = { NULL, NULL, NULL, NULL, "<", ">", "DPG", NULL, NULL, NULL, "UP", "DN"};
	return (bt < 12 ? label[bt] : 0);
}

int LifeMFD::ButtonMenu(const MFDBUTTONMENU** menu) const
{
	// The menu descriptions for the two buttons
	static const MFDBUTTONMENU mnu[12] = {
		{NULL, 0, NULL},
		{NULL, 0, NULL},
		{NULL, 0, NULL},
		{NULL, 0, NULL},
		{">", 0, '>'},
		{"<", 0, '<'},
		{"DamPinG Seat Belt", 0, 'G'},
		{NULL, 0, NULL},
		{NULL, 0, NULL},
		{NULL, 0, NULL},
		{"UP", 0, 'U'},
		{"DN", 0, 'D'},
	};
	if (menu) *menu = mnu;
	return 12; // return the number of buttons used
}

// MFD message parser
int LifeMFD::MsgProc(UINT msg, UINT mfd, WPARAM wparam, LPARAM lparam)
{
	switch (msg) {
	/*case OAPI_MSG_MFD_OPENED:
		// Our new MFD mode has been selected, so we create the MFD mode and
		// return a pointer to it.
		return (int)(new LifeMFD(LOWORD(wparam), HIWORD(wparam), (VESSEL*)lparam));
	*/
	case OAPI_MSG_MFD_OPENEDEX: {
		MFDMODEOPENSPEC* ospec = (MFDMODEOPENSPEC*)wparam;
		return (int)(new LifeMFD(ospec->w, ospec->h, (VESSEL*)lparam));
		}
	}
	return 0;
}

bool LifeMFD::ConsumeKeyBuffered (DWORD key)
{
	switch (key) {
	case OAPI_KEY_G:
		seatBeltFasten = !seatBeltFasten;
		// draw flag "Damping DISABLED" (yellow) ou "Damping Active" (vert)
		return true;
	}
	return false;
}

bool LifeMFD::ConsumeButton (int bt, int event)
{
	if (!(event & PANEL_MOUSE_LBDOWN)) return false;
	static const DWORD btkey[12] = { NULL,NULL,NULL,NULL,NULL,NULL,
								OAPI_KEY_G, NULL, NULL, NULL, NULL, NULL };
	if (bt < 12) return ConsumeKeyBuffered (btkey[bt]);
	else return false;
}

// Repaint the MFD
bool LifeMFD::Update(oapi::Sketchpad* skp)
{	// skp = drawing object for the MFD mode, not compatible with GraphMFD?
	char* flagDamping;
	long   flagLength;

	Title(skp, "LIFE Support");

	warningPen = GetDefaultPen(1); // yellow
	neutralPen = GetDefaultPen(2); // white
	redPen = GetDefaultPen(3); // red
	defaultPen = skp->SetPen(neutralPen);
	dfltBrsh = skp->SetBrush(grnBrush);

	// FLAG "DAMPING"
	paintDampingFlag(skp);

	// G-GAUGE
	paintGGauge(skp);

	// Autonomy Area
	paintAutonomyLevels(skp);

	return true;
}

void LifeMFD::WriteStatus (FILEHANDLE scn) const
{
	char cbuf[256];
	oapiWriteScenario_int (scn, "PAGE", page);
	//if (log_auto) strcpy (cbuf, "AUTO");
	//else sprintf (cbuf, "%0.1f %0.1f", graph[g_weight].data_min, graph[g_weight].data_max);
	oapiWriteScenario_string (scn, "WEIGHT_RANGE", cbuf);
}

void LifeMFD::ReadStatus (FILEHANDLE scn)
{
    char *line;
	while (oapiReadScenario_nextline (scn, line)) {
		// syntax: _strnicmp=0 if line(1:4) is identical to "PAGE"(1:4).
		if (!_strnicmp (line, "PAGE", 4))
			sscanf (line+5, "%d", &page);
		//else if (!_strnicmp (line, "WEIGHT_RANGE", 12))
		//	SetLogRange(line+13);
	}
}

void LifeMFD::StoreStatus (void) const
{
	saveprm.page     = page;
	//saveprm.logmin   = (log_auto ? 0.0f : graph[g_weight].data_min);
	//saveprm.logmax   = (log_auto ? 0.0f : graph[g_weight].data_max);
	//saveprm.linmin   = (lin_auto ? 0.0f : graph[1].data_min);
	//saveprm.linmax   = (lin_auto ? 0.0f : graph[1].data_max);
}

void LifeMFD::RecallStatus (void)
{
	char cbuf[128];
	page = saveprm.page;
	if (saveprm.logmin < saveprm.logmax)
		sprintf (cbuf, "%f %f", saveprm.logmin, saveprm.logmax);
	else strcpy (cbuf, "AUTO");
	//SetLogRange (cbuf);
	if (saveprm.linmin < saveprm.linmax)
		sprintf (cbuf, "%f %f", saveprm.linmin, saveprm.linmax);
	else strcpy (cbuf, "AUTO");
	//SetLinRange (cbuf);
}

LifeMFD::SavePrm LifeMFD::saveprm = {0, 0.0, 0.0, 0.0, 0.0};

//----------- Specific LifeMFD members ----------------

void LifeMFD::paintDampingFlag(oapi::Sketchpad* skp)
{
	char* flagDamping;
	long  flagLength, hh, textWidth;
	skp->SetFont(fontB);

	skp->SetTextAlign(oapi::Sketchpad::CENTER, oapi::Sketchpad::TOP);
	if (seatBeltFasten) {
		skp->SetTextColor(0x00FF00);
		skp->SetBrush(0);
		flagDamping = "DAMPING";
		flagLength = 7;
		skp->SetPen(defaultPen);
	}
	else {
		skp->SetTextColor(0x000000); // black (on yellow)
		flagDamping = "No DAMPING";
		flagLength = 10;
		skp->SetBrush(ylwBrush);
		skp->SetPen(warningPen);
	}
	textWidth = (long)skp->GetTextWidth(flagDamping, 0);
	hh = (long)LOWORD(skp->GetCharSize());
	skp->Rectangle(W - 5 - textWidth *1.2, 5, W - 5, 7 + hh);
	skp->Text(W - 5 - textWidth *1.2/2, 4 + 1, flagDamping, flagLength);
}

void LifeMFD::paintGGauge(oapi::Sketchpad* skp)
{
	static oapi::IVECTOR2 pt[4];
	long cc = (long)cw;
	long hh = (long)ch;
	long X0 = 2*cc;  // left margin
	long Y0 = 2.5*hh; // top margin
	long gWidth = max(12*cc, (W / 4 - X0 - 2 * cc)); // Total width for 3 gauges
	long gHeight = H/2 - Y0 - hh; // gauge max height
	long baseLevel = Y0 + gHeight;

	char str[256] = "";
	long gaugeWidths[4] = { gWidth*3/5, gWidth/5, gWidth/5, gWidth/15 };
	long gaugeCenters[4] = {X0 + cc+ gaugeWidths[0]/2,
						 X0 + cc + gaugeWidths[0] + gaugeWidths[1]/ 2,
						 X0 + cc + gaugeWidths[0] + gaugeWidths[1] + gaugeWidths[2] / 2,
						 X0 + cc + gaugeWidths[0] + gaugeWidths[1] + gaugeWidths[2] / 2 };
	float llGs[4] = { cumulG7d / LENGTH7D, cumulG1h / LENGTH1H, life.g5s, life.gc };
	float llnaV, Gs;

	skp->SetTextAlign(oapi::Sketchpad::CENTER, oapi::Sketchpad::BOTTOM);
	for (int i = 0; i < 4; i++) {
		Gs = llGs[i];
		llnaV = min(3, (Gs < 1 ? Gs : 1 + 2 * log10((long double)Gs))); // [0..1] U [1..3] for naV<1G up to naV=10G
		sprintf(str, "%3.1f", Gs);
		skp->SetBrush(grnBrush);
		skp->SetTextColor(0x00FF00);
		if (Gs < 0.1) { strcpy(str, " ~0"); }
		if (Gs >= 10) { strcpy(str, "10+"); }
		// 1-week average
		if (i == 0 & (Gs < GLIMIT7D || Gs > GLIMIT1H)) { skp->SetBrush(ylwBrush); skp->SetTextColor(DKYELLOW); }
		// 1-hour average
		if (i == 1 & (Gs > GLIMIT1H)) { skp->SetBrush(ylwBrush); skp->SetTextColor(DKYELLOW); }
		// 5-s average
		if (i == 2 & (Gs > GLIMIT5S)) { skp->SetBrush(ylwBrush); skp->SetTextColor(DKYELLOW); }
		if (i == 3 & (Gs > GLIMIT00)) { skp->SetBrush(redBrush); skp->SetTextColor(0x0000FF); }

		// ==== gauges ====
		skp->SetPen(neutralPen);
		// instantenous
		pt[0] = { gaugeCenters[i]-gaugeWidths[i]/2, baseLevel };
		pt[1] = { gaugeCenters[i]+gaugeWidths[i]/2, baseLevel };
		pt[2] = { gaugeCenters[i]+gaugeWidths[i]/2, baseLevel - (long)(llnaV * gHeight / 3) };
		pt[3] = { gaugeCenters[i]-gaugeWidths[i]/2, baseLevel - (long)(llnaV * gHeight / 3) };
		skp->Polygon(pt, 4);

		// Numbers
		skp->SetFont(fontN);
		if (i == 3)
			 { skp->Text(gaugeCenters[i], Y0+hh, str, strlen(str));}
		else { skp->Text(gaugeCenters[i], baseLevel + hh, str, strlen(str)); }
	}

	// Scale
	skp->SetTextColor(0xFFFFFF);
	skp->Text(X0 - cc, Y0, "10", 2);
	skp->Text(X0 - cc, baseLevel - gHeight / 3, "1", 1);
	skp->Text(X0 - cc, baseLevel, "0", 1);
	//skp->Text(X0, Y0+hh, "[ G ]", 5);
	skp->Text(gaugeCenters[3], Y0-1, "[ G ]", 5);

	// 1G-line
	skp->MoveTo(X0 - cc, baseLevel - gHeight);
	skp->LineTo(X0 + gWidth + cc, baseLevel - gHeight);
	skp->MoveTo(X0 - cc, baseLevel - gHeight / 3);
	skp->LineTo(X0 + gWidth + cc, baseLevel - gHeight / 3);
	skp->MoveTo(X0 - cc, baseLevel);
	skp->LineTo(X0 + gWidth + cc, baseLevel);

	// Warnings
	if (trauma) {
		strcpy(str, "TRAUMA");
	}
	else if (veilRisk) {
		strcpy(str, "FAINTG");
	}
	else if (cardioRisk) {
		strcpy(str, "CARDIO");
	}
	else if (skeletRisk) {
		strcpy(str, "SKELET");
	}
	if (trauma || veilRisk || cardioRisk || skeletRisk) {
		skp->SetBrush(ylwBrush);
		skp->SetTextColor(0x000000);
		skp->Rectangle(X0 + gWidth / 2 - 4 * cc, Y0 + 2, X0 + gWidth / 2 + 4 * cc, Y0 + hh + 1);
		skp->Text(X0 + gWidth / 2, Y0 + hh+1, str, strlen(str));
	}

}

void LifeMFD::paintAutonomyLevels(oapi::Sketchpad* skp)
{
	static oapi::IVECTOR2 pt[4];
	long cc = (long)cw;
	long hh = (long)ch;
	long X0 = hh;  // left margin after 1*height (vertical writing)
	long Y0 = H/2 +2; // top margin = 2nd half of MFD + 2 pixels
	long gWidth  = max(14*cc - hh,(W / 3 -hh-cc))/3; // gauges width
	long gHeight = H - Y0 - 2.5*hh; // gauge max height
	char strT[10] = "";
	char strN[10] = "";
	//int   strLength[3] = { 3,5,4 }; // 'AIR', 'WATER', 'FOOD' lengths
	long fullGauge = Y0 + 2 * hh + 1; // default height of horizontal white|red line

	// rgLL is the level (duration) of remaining resource in logarithmic scale
	double rgL, cHH; // current resource's levels in [d] and Height [px]
	float rgLL[3] = {
		log10((long double)(1 + life.rgAir / (life.cdAir * life.nPax))),
		log10((long double)(1 + life.rgWater / (life.cdWater * life.nPax))),
		log10((long double)(1 + life.rgFood / (life.cdFood * life.nPax))) };
	float mxLL = 0.1; // to avoid division by zero
	for (int i = 0; i < 3; i++) mxLL = max(mxLL, rgLL[i]); // mxLL is the highest level of all resources

	// woLL is the duration without resource in % of capability without that resource
	long zHH = 2*hh; // height of the "lacking" gauge if any is to be presented
	long cWO; // current lack of resource
	float woLL[3] = { // % of lacking resource
		dwoR[0] / (life.woAir / life.nPax),
		dwoR[1] / life.woWater,
		dwoR[2] / life.woFood };
	float nvLL = -1; // if nvLL<0, do nothing
	for (int i = 0; i < 3; i++) nvLL = min(1, max(nvLL, woLL[i])); // if not, nvLL is the highest % (i.e. most urgent problem)

	skp->SetFont(fontN);
	// painting 3 gauges
	skp->SetTextAlign(oapi::Sketchpad::CENTER, oapi::Sketchpad::TOP);
	for (int i = 0; i < 3; i++)
	{
		switch (i) {
		case 0:
			strcpy(strT, "AIR");
			rgL = life.rgAir / (life.cdAir * life.nPax);
			cWO = (nvLL < 0 ? 0 : (long)(woLL[i] * zHH));
			break;
		case 1:
			strcpy(strT, "WATER");
			rgL = life.rgWater / (life.cdWater * life.nPax);
			cWO = (nvLL < 0 ? 0 : (long)(woLL[i] * zHH));
			break;
		case 2:
			strcpy(strT, "FOOD");
			rgL = life.rgFood / (life.cdFood * life.nPax);
			cWO = (nvLL < 0 ? 0 : (long)(woLL[i] * zHH));
		}
		// format + text writing
		if (rgL >= 699) sprintf(strN, "%3.1fy", rgL / 365.25);
		else if (rgL >= 100) sprintf(strN, "%3.1fw", rgL / 7);
		else if (rgL >= 2) sprintf(strN, "%3.1fd", rgL);
		else if (dwoR[i] < 0) sprintf(strN, "%3.1fH", rgL * 24); // dwoR < 0 means there is still some resources
		else if (woLL[i] > 1) strcpy(strN, "--");
		else sprintf(strN, "%3.0f%%", 100 * (double)woLL[i]);

		// color
		if (woLL[i] >= 0.5) skp->SetTextColor(GWARNING);
		else if (rgL < 1) skp->SetTextColor(DKYELLOW);
		else skp->SetTextColor(0xFFFFFF);
		skp->Text((long)(X0 + i * (gWidth + 0.5*cc) + gWidth / 2), Y0   , strT, strlen(strT));
		if (dwoR[i] < 0)
			skp->Text((long)(X0 + i * (gWidth + 0.5 * cc) + gWidth / 2), Y0 + hh, strN, strlen(strN));
		else
			skp->Text((long)(X0 + i * (gWidth + 0.5 * cc) + gWidth / 2), Y0 + hh, "--", 2);
			skp->Text((long)(X0 + i * (gWidth + 0.5 * cc) + gWidth / 2),
				fullGauge + gHeight - hh -(zHH+hh), strN, strlen(strN));
		// drawing
		if (rgL < 1) {
				 skp->SetPen(warningPen); skp->SetBrush(ylwBrush);
		} else { skp->SetPen(neutralPen); skp->SetBrush(grnBrush); }
		// if any of the resources is lacking (nvLL>=0), then reduce the gauges (and add a red under-layer below the gauges) 
		cHH = gHeight - hh - (nvLL < 0 ? 0 : zHH);
		pt[0] = { (long)(X0 + i * (gWidth + 0.5 * cc)         ), fullGauge + (long)(cHH * (1 - rgLL[i] / mxLL)) };
		pt[1] = { (long)(X0 + i * (gWidth + 0.5 * cc) + gWidth), fullGauge + (long)(cHH * (1 - rgLL[i] / mxLL)) };
		pt[2] = { (long)(X0 + i * (gWidth + 0.5 * cc) + gWidth), fullGauge + (long)cHH };
		pt[3] = { (long)(X0 + i * (gWidth + 0.5 * cc)         ), fullGauge + (long)cHH };
		skp->Polygon(pt, 4);
		// then, if any of the resources is lacking (nvLL>=0), add a red under-layer below the needed gauges 
		if (dwoR[i] >= 0) {
			// case
			skp->SetBrush(0x0);
			skp->SetPen(redPen);
			skp->Rectangle((long)(X0 + i * (gWidth + 0.5 * cc)), fullGauge + gHeight - hh - zHH,
				(long)(X0 + i * (gWidth + 0.5 * cc) + gWidth),   fullGauge + gHeight - hh);
			// filled
			if (woLL[i] < 0.5) skp->SetBrush(ylwBrush);
			else skp->SetBrush(redBrush);
			pt[0] = { (long)(X0 + i * (gWidth + 0.5 * cc)),
				fullGauge + gHeight - hh - zHH };
			pt[1] = { (long)(X0 + i * (gWidth + 0.5 * cc) + gWidth),
				fullGauge + gHeight - hh - zHH };
			pt[2] = { (long)(X0 + i * (gWidth + 0.5 * cc) + gWidth),
				fullGauge + gHeight - hh - (long)((1 - woLL[i]) * zHH) };
			pt[3] = { (long)(X0 + i * (gWidth + 0.5 * cc)),
				fullGauge + gHeight - hh - (long)((1 - woLL[i]) * zHH) };
			skp->Polygon(pt, 4);
		}
	}
	// horizontal lines
	skp->SetPen(neutralPen);
	if (nvLL >= 0) {
		skp->SetPen(redPen);
		skp->MoveTo(X0 - 2, fullGauge + gHeight - hh - zHH);
		skp->LineTo(X0 + 3 * gWidth + cc + 2, fullGauge + gHeight - hh - zHH);
	}
	skp->MoveTo(X0 - 2, fullGauge);
	skp->LineTo(X0 + 3 * gWidth + cc + 2, fullGauge);


	// vertical text
	skp->SetTextAlign(oapi::Sketchpad::RIGHT, oapi::Sketchpad::TOP);
	skp->SetFont(fontV);
	strcpy(strT, "AUTONOMY");
	skp->SetTextColor(0xFFFFFF);
	skp->Text(X0 - hh, Y0+2.5*hh, strT, strlen(strT));
}